/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facturacion;

import Design.Home;
import Design.NuevoCliente;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.WindowConstants;

/**
 *
 * @author Matt Screak
 */
public class Controler {
    public Connection con;
    private Cliente clienteActual;
    
    public Controler(Connection con) {
        this.con = con;
    }
    
    
    public void openNewClient(Home h){
        NuevoCliente newClient = new NuevoCliente(this);
        newClient.setVisible(true);
        newClient.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        newClient.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                newClient.dispose();
                h.setEnabled(true);
                h.toFront();
                h.requestFocus();
            }
        });
        
    }
    
    public String getClienteActualName (){
        return clienteActual.getNombre();
    }
    
    public SqlTest getConnection(){
        SqlTest sql = new SqlTest();
        return sql;
    }
   
    public void giveData(String nombre, String cuil, String direccion, String numDir, String numTel, String mail) throws SQLException{
        Statement stmt = con.createStatement();
        String Query = "INSERT INTO `sqltest`.`test` (`nombre`, `cuil`, `direccion`, `num.dir`, `num.tel`, `mail`) VALUES(?, ?, ?, ?, ?, ?);";
        PreparedStatement statement = con.prepareStatement(Query);
        statement.setString(1, nombre);
        statement.setString(2, cuil);
        statement.setString(3, direccion);
        statement.setString(4,numDir);
        statement.setString(5,numTel);
        statement.setString(6,mail);
        statement.execute();
    }
    
}