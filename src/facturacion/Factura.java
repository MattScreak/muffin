/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facturacion;

/**
 *
 * @author Matt Screak
 */
public class Factura {
    private int numero_factura;
    private String fecha_emision = "";
    private double monto;

    public Factura(int numero_factura, double monto) {
        this.numero_factura = numero_factura;
        Fecha fecha = new Fecha();
        this.fecha_emision = fecha.getFechaActual();
        this.monto = monto;
    }

    public String getInfo(){
        return "Numero de Factura: "+ numero_factura + "\nFecha de emision: " + fecha_emision + "\tMonto: " + monto + "\n\n";
    }
    
    public int getNumero_factura() {
        return numero_factura;
    }

    public String getFecha_emision() {
        return fecha_emision;
    }

    public double getMonto() {
        return monto;
    }
    
    
}
