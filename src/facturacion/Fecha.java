/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facturacion;

import java.util.Calendar;
import java.util.Formatter;
import java.util.GregorianCalendar;

/**
 *
 * @author Matt Screak
 */
public class Fecha {
    private int ano;
    private int mes;
    private int dia;

    public Fecha() {
        Calendar c = new GregorianCalendar();
        ano = c.get(Calendar.YEAR);
        mes = c.get(Calendar.MONTH);
        dia = c.get(Calendar.DAY_OF_MONTH);
    }

    
    public String getFechaActual() {
        //return Integer.toString((dia*1000000) + (mes*10000) + (ano)) ;
        return Integer.toString(dia) + "/" + Integer.toString(mes) + "/" + Integer.toString(ano);
    }
    
}
