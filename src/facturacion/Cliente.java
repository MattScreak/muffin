/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facturacion;

import java.util.ArrayList;

/**
 *
 * @author Matt Screak
 */
public class Cliente {
    private int id;
    private String cuil;
    private String nombre;
    private ArrayList<Factura> facturas;

    //Constructor
    
    //Methods
    
    public void newFactura(int facturaNum,double monto){
        Factura nuevaFactura = new Factura(facturaNum, monto);
        facturas.add(nuevaFactura);
    }
    
    public double getTotal(){
        double total = 0.0;
        for(int i=0; i<facturas.size(); i++){
            total += facturas.get(i).getMonto();
        }
        return total;
    }
    
    public void paga(int facturaNum, double monto){
        this.newFactura(facturaNum, -monto);
    }
    
    public String getFacturas(){
        String info = "";
        for(int i=0; i<facturas.size(); i++){
            info = info + (facturas.get(i).getInfo());
        }
        return info;
    }
    
    //Get and Set
    
    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
    
    
   
}
